//
// Created by Andreas on 19/09/2021.
//

#ifndef KEY_VALUE_STORE_FILEBACKEDKVSTORE_H
#define KEY_VALUE_STORE_FILEBACKEDKVSTORE_H

#include "KvStoreInterface.h"
#include "FactoryInterface.h"

#include <boost/interprocess/containers/map.hpp>
#include <boost/interprocess/containers/string.hpp>
#include <boost/interprocess/managed_mapped_file.hpp>

#include <shared_mutex>
#include <unordered_map>
#include <unordered_set>

class FileBackedKvStore : public KvStoreExtendedInterface {
    std::shared_mutex mutable mutex;
    std::size_t maxElementsRead;
    std::size_t maxElementsMutate;
    std::size_t mutable writtenSize = {};
    using StdMap = std::unordered_map<std::string, std::string>;
    std::unordered_map<std::string, std::optional<std::string>> mutable readCache;
    StdMap mutable writeCache;
    using StdSet = std::unordered_set<std::string>;
    StdSet mutable eraseCache;

    using ReadGuard = std::shared_lock<std::shared_mutex>;
    using FullGuard = std::lock_guard<std::shared_mutex>;

public:
    using ImplHandleFactory = std::unique_ptr<FactoryInterface<std::unique_ptr<KvStoreExtendedInterface>>>;
private:
    ImplHandleFactory mapFactory;

    std::unique_ptr<KvStoreExtendedInterface> getProxy() const;

    // Randomly delete one element.
    // It's not the smartest, but definitely the simplest cache eviction algorithm.
    void evictOneFromReadCache() const;

public:
    FileBackedKvStore(FileBackedKvStore::ImplHandleFactory factory, std::size_t readSize, std::size_t mutateSize);

    // NB this is just an estimate!
    std::size_t size() const override;

    std::optional<std::string> get(std::string const& key) const override;

    void put(std::string const& key, std::string const& val) override;

    void erase(std::string const& key) override;

    void filter(std::function<bool(std::string const&)> predicate, std::shared_ptr<KvStoreInterface> other) override;

    void addAllTo(std::shared_ptr<KvStoreInterface> other) override;
};

#endif //KEY_VALUE_STORE_FILEBACKEDKVSTORE_H
