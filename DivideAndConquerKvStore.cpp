//
// Created by Andreas on 20/09/2021.
//

#include "DivideAndConquerKvStore.h"

#include <map>
#include <cassert>
#include <numeric>

DivideAndConquerKvStore::Children::const_iterator DivideAndConquerKvStore::getChildAndRebalanceIfNecessary(DivideAndConquerKvStore::Hash hash) {
    auto child = getChild(hash);
    // if the size is at max, divide into two
    if (auto const childSize = child->second->size(); childSize >= maxElementsPerChild) {
        // get new hash
        auto additionalHash = std::size_t{};
        if (children.size() < 2) {
            additionalHash = std::size_t{1} << (std::numeric_limits<std::size_t>::digits-1);
        }
        else {
            additionalHash = hashBetween(prevOrLastChild(child)->first, child->first);
        }
        // create new child with that hash
        auto [additionalChild, additionalChildInserted] = children.emplace(additionalHash, factory->construct(additionalHash));
        assert(additionalChildInserted);
        // divide
        child->second->filter([additionalChild,this](auto&& p) {
                                  return getChild(hasher(p)) == additionalChild;
                              },
                              additionalChild->second);
        // return correct child
        return getChild(hash);
    }
        // if the size of this and the next combined is smaller than mergeThreshold, then merge
    else if (auto const nextChild = nextOrFirstChild(child); children.size() > 1 && childSize + nextChild->second->size() < mergeThreshold) {
        nextChild->second->addAllTo(child->second);
        children.erase(child);
        // return correct child
        return nextChild;
    }
    else {
        return child;
    }
}

std::size_t DivideAndConquerKvStore::size() const {
    auto lockGuard = ReadGuard(mutex);
    return std::accumulate(begin(children),
                           end(children),
                           std::size_t{},
                           [](auto acc, auto&& child) { return acc + child.second->size(); });
}

std::optional<std::string> DivideAndConquerKvStore::get(const std::string &key) const {
    auto hash = hasher(key);
    auto lockGuard = ReadGuard(mutex);
    auto child = getChild(hash);
    return child->second->get(key);
}

void DivideAndConquerKvStore::put(const std::string &key, const std::string &val) {
    auto hash = hasher(key);
    auto lockGuard = FullGuard(mutex);
    auto child = getChildAndRebalanceIfNecessary(hash);
    child->second->put(key, val);
}

void DivideAndConquerKvStore::erase(const std::string &key) {
    auto hash = hasher(key);
    auto lockGuard = FullGuard(mutex);
    auto child = getChildAndRebalanceIfNecessary(hash);
    child->second->erase(key);
}

DivideAndConquerKvStore::Children::const_iterator DivideAndConquerKvStore::getChild(DivideAndConquerKvStore::Hash hash) const {
    auto result = children.lower_bound(hash);
    return result == end(children) ? begin(children) : result;
}

std::size_t DivideAndConquerKvStore::hashBetween(DivideAndConquerKvStore::Hash lhs, DivideAndConquerKvStore::Hash rhs) {
    auto result = lhs + ((rhs - lhs) >> 1);
    return result;
}

DivideAndConquerKvStore::Children::const_iterator  DivideAndConquerKvStore::nextOrFirstChild(DivideAndConquerKvStore::Children::const_iterator child) const {
    auto result = next(child);
    return result == end(children) ? begin(children) : result;
}

DivideAndConquerKvStore::Children::const_iterator  DivideAndConquerKvStore::prevOrLastChild(DivideAndConquerKvStore::Children::const_iterator child) const {
    return child == begin(children) ? --end(children) : prev(child);
}

DivideAndConquerKvStore::DivideAndConquerKvStore(std::size_t maxPerChild, std::size_t mergeThreshold,
                                                 DivideAndConquerKvStore::KvStoreFactory factory) :
    maxElementsPerChild(maxPerChild),
    mergeThreshold(mergeThreshold),
    factory(std::move(factory)),
    children{{{}, this->factory->construct({})}}
{}
