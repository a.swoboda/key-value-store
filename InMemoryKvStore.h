//
// Created by Andreas on 20/09/2021.
//

#ifndef KEY_VALUE_STORE_INMEMORYKVSTORE_H
#define KEY_VALUE_STORE_INMEMORYKVSTORE_H

#include "KvStoreInterface.h"

#include <map>
#include <shared_mutex>

class InMemoryKvStore : public KvStoreExtendedInterface {
    std::map<std::string, std::string> map;
    std::shared_mutex mutable mutex;

    using ReadGuard = std::shared_lock<std::shared_mutex>;
    using FullGuard = std::lock_guard<std::shared_mutex>;

public:
    std::size_t size() const override;

    std::optional<std::string> get(std::string const& key) const override;

    void put(std::string const& key, std::string const& val) override;

    void erase(std::string const& key) override;

    void filter(std::function<bool(std::string const&)> predicate, std::shared_ptr<KvStoreInterface> other) override;
    void addAllTo(std::shared_ptr<KvStoreInterface> other) override;
};


#endif //KEY_VALUE_STORE_INMEMORYKVSTORE_H
