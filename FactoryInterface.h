//
// Created by Andreas on 20/09/2021.
//

#ifndef KEY_VALUE_STORE_FACTORYINTERFACE_H
#define KEY_VALUE_STORE_FACTORYINTERFACE_H

template <typename T, typename...Params>
struct FactoryInterface {
    virtual ~FactoryInterface() = default;
    virtual T construct(Params...) const = 0;
};

#endif //KEY_VALUE_STORE_FACTORYINTERFACE_H
