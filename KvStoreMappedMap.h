//
// Created by Andreas on 20/09/2021.
//

#ifndef KEY_VALUE_STORE_KVSTOREMAPPEDMAP_H
#define KEY_VALUE_STORE_KVSTOREMAPPEDMAP_H

#include "KvStoreInterface.h"

#include "FactoryInterface.h"

#include <boost/interprocess/managed_mapped_file.hpp>
#include <boost/interprocess/containers/string.hpp>
#include <boost/interprocess/containers/map.hpp>
#include <utility>


class KvStoreMappedMap;

class MappedMapFactory : public FactoryInterface<std::unique_ptr<KvStoreExtendedInterface>> {
    std::string const filename;
public:
    MappedMapFactory(std::string name, std::size_t filesize);
    ~MappedMapFactory() override;

    [[nodiscard]] std::unique_ptr<KvStoreExtendedInterface> construct() const override;
};


class KvStoreMappedMap : public KvStoreExtendedInterface {
    using MappedFile = boost::interprocess::managed_mapped_file;
    MappedFile mutable mappedFile;
    template <typename T>
    using AllocatorT = boost::interprocess::allocator<T, boost::interprocess::managed_mapped_file::segment_manager>;
    using CharAllocator = AllocatorT<char>;
    using StringType = boost::interprocess::basic_string<char, std::char_traits<char>, CharAllocator>;
    using PairAllocator = AllocatorT<std::pair<StringType const, StringType>>;
    using StringLess = std::less<StringType>;
    using MapType = boost::interprocess::map<StringType, StringType, StringLess, PairAllocator>;
    MapType& data;

    static constexpr auto OBJECT_NAME = "map";
public:
    explicit KvStoreMappedMap(MappedFile&& file);

    std::size_t size() const override;

    std::optional<std::string> get(std::string const& key) const override;

    void put(std::string const& key, std::string const& val) override;

    void erase(std::string const& key) override;

    void filter(std::function<bool(std::string const&)> predicate, std::shared_ptr<KvStoreInterface> other) override;

    void addAllTo(std::shared_ptr<KvStoreInterface> other) override;
};


#endif //KEY_VALUE_STORE_KVSTOREMAPPEDMAP_H
