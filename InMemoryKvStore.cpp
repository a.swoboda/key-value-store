//
// Created by Andreas on 20/09/2021.
//

#include "InMemoryKvStore.h"

std::size_t InMemoryKvStore::size() const {
    auto lockGuard = ReadGuard(mutex);
    return map.size();
}

std::optional<std::string> InMemoryKvStore::get(const std::string &key) const {
    auto lockGuard = ReadGuard(mutex);
    auto it = map.find(key);
    if (it == end(map)) { return {}; }
    return it->second;
}

void InMemoryKvStore::put(const std::string &key, const std::string &val) {
    auto lockGuard = FullGuard(mutex);
    map.insert_or_assign(key, val);
}

void InMemoryKvStore::erase(const std::string &key) {
    map.erase(key);
}

void
InMemoryKvStore::filter(std::function<bool(const std::string &)> predicate, std::shared_ptr<KvStoreInterface> other) {
    for (auto it = begin(map); it != end(map); /* increment in body */) {
        if (predicate(it->first)) {
            other->put(it->first, it->second);
            it = map.erase(it);
        }
        else {
            ++it;
        }
    }
}

void InMemoryKvStore::addAllTo(std::shared_ptr<KvStoreInterface> other) {
    for (auto&& p : map) {
        other->put(p.first, p.second);
    }
}
