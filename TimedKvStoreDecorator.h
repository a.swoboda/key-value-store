//
// Created by Andreas on 22/09/2021.
//

#ifndef KEY_VALUE_STORE_TIMEDKVSTOREDECORATOR_H
#define KEY_VALUE_STORE_TIMEDKVSTOREDECORATOR_H

#include "KvStoreInterface.h"

#include <chrono>
#include <atomic>
#include <string>

#include <iostream>

class TimedKvStoreDecorator : public KvStoreInterface {
    using Clock = std::chrono::steady_clock;
    using ClockCounter = std::atomic<typename Clock::duration::rep>;
    ClockCounter mutable totalTime;

    std::unique_ptr<KvStoreInterface> const pimpl;

    struct LifetimeCounter {
        ClockCounter& acc;
        Clock::time_point start;
        explicit LifetimeCounter(ClockCounter& acc) :
            acc(acc), start(Clock::now()) {}
        ~LifetimeCounter() {
            auto duration = Clock::now() - start;
            acc.fetch_add(duration.count());
        }
        LifetimeCounter(LifetimeCounter const& other) = delete;
        LifetimeCounter(LifetimeCounter&& other) = delete;
        LifetimeCounter& operator =(LifetimeCounter const& rhs) = delete;
        LifetimeCounter& operator =(LifetimeCounter&& rhs) = delete;
    };
public:
    explicit TimedKvStoreDecorator(std::unique_ptr<KvStoreInterface> pimpl) :
        pimpl(std::move(pimpl))
    {}

    Clock::duration duration() const {
        return Clock::duration(totalTime.load());
    }

    void reset() const {
        totalTime.store({});
    }

    std::optional<std::string> get(std::string const& key) const override {
        auto timer = LifetimeCounter(totalTime);
        return pimpl->get(key);
    }
    void put(std::string const& key, std::string const& val) override {
        auto timer = LifetimeCounter(totalTime);
        pimpl->put(key, val);
    }
    void erase(std::string const& key) override {
        auto timer = LifetimeCounter(totalTime);
        pimpl->erase(key);
    }
    std::size_t size() const override {
        auto timer = LifetimeCounter(totalTime);
        return pimpl->size();
    }
};


#endif //KEY_VALUE_STORE_TIMEDKVSTOREDECORATOR_H
