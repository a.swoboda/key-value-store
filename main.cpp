#include "FileBackedKvStore.h"
#include "InMemoryKvStore.h"
#include "DivideAndConquerKvStore.h"
#include "KvStoreMappedMap.h"

#include "TimedKvStoreDecorator.h"

#include <iostream>
#include <string>
#include <algorithm>

#include <map>
#include <cassert>

std::string random_string( size_t length )
{
    auto randchar = []() -> char
    {
        const char charset[] =
            "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
    };
    std::string str(length,0);
    std::generate_n( str.begin(), length, randchar );
    return str;
}


int main() {
    struct ExtendedFactory : FactoryInterface<std::unique_ptr<KvStoreExtendedInterface>, std::size_t> {
        [[nodiscard]] std::unique_ptr<KvStoreExtendedInterface> construct(std::size_t hash) const override {
            return std::make_unique<FileBackedKvStore>(std::make_unique<MappedMapFactory>(std::to_string(hash), 1 << 26), 32, 32);
        }
    };
    static constexpr std::size_t maxPerChild = 1 << 10;
    static constexpr std::size_t mergeThreshold = 1 << 8;
    auto dividingStore = std::make_unique<DivideAndConquerKvStore>(
            maxPerChild,
            mergeThreshold,
            std::make_unique<ExtendedFactory>());
    auto divideAndConquerStore = TimedKvStoreDecorator(std::move(dividingStore));
    auto referenceImpl = TimedKvStoreDecorator(std::make_unique<InMemoryKvStore>());
    auto used = std::map<std::string, std::string>{};
    auto nTestStrings = 1 << 14;
    for (auto i = 0; i != nTestStrings; ++i) {
        static constexpr auto stringSize = 1000;
        auto const testKey0 = random_string(stringSize);
        auto const testVal0 = random_string(stringSize);
        divideAndConquerStore.put(testKey0, testVal0);
        referenceImpl.put(testKey0, testVal0);
        auto const testKey1 = random_string(stringSize);
        auto const testVal1 = random_string(stringSize);
        divideAndConquerStore.put(testKey1, testVal0);
        referenceImpl.put(testKey1, testVal0);
        divideAndConquerStore.erase(testKey0);
        referenceImpl.erase(testKey0);
        divideAndConquerStore.put(testKey1, testVal1);
        referenceImpl.put(testKey1, testVal1);
        used.emplace(testKey1, testVal1);
        if (i % 1'000 == 999) {
            std::cout << i+1 << std::endl;
        }
    }

    auto const toMs = [](auto d) { return std::chrono::duration_cast<std::chrono::milliseconds>(d).count(); };

    std::cout << "\nwriting took (ms):"
              << "\ndivide: " << toMs(divideAndConquerStore.duration())
              << "\nreference: " << toMs(referenceImpl.duration()) << '\n';

    divideAndConquerStore.reset();
    referenceImpl.reset();

    for (auto&& p : used) {
        if (auto const saved = referenceImpl.get(p.first); saved != p.second) {
            std::cout << "ref: " << p.first << ' ' << p.second << ' ' << saved.value_or("x") << '\n';
        }
        if (auto const expected = divideAndConquerStore.get(p.first); expected != p.second) {
            std::cout << "dac: " << p.first << ' ' << p.second << ' ' << expected.value_or("x") << '\n';
        }
    }

    std::cout << "\nreading took (ms):"
              << "\ndivide: " << toMs(divideAndConquerStore.duration())
              << "\nreference: " << toMs(referenceImpl.duration()) << '\n';

    std::cout << "\ntotal keys used: " << used.size()
        << "\nkeys in divide: " << divideAndConquerStore.size()
        << "\nkeys in ref: " << referenceImpl.size()
        << '\n';

    return 0;
}
