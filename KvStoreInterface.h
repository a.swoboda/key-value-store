//
// Created by Andreas on 19/09/2021.
//

#ifndef KEY_VALUE_STORE_KVSTOREINTERFACE_H
#define KEY_VALUE_STORE_KVSTOREINTERFACE_H

#include <functional>
#include <memory>
#include <optional>
#include <string>
#include <utility>

struct KvStoreInterface {
    virtual ~KvStoreInterface() = default;
    [[nodiscard]] virtual std::optional<std::string> get(std::string const& key) const = 0;
    virtual void put(std::string const& key, std::string const& val) = 0;
    virtual void erase(std::string const& key) = 0;
    [[nodiscard]] virtual std::size_t size() const = 0;
};

struct KvStoreExtendedInterface : KvStoreInterface {
    virtual void filter(std::function<bool(std::string const&)> predicate, std::shared_ptr<KvStoreInterface> other) = 0;
    virtual void addAllTo(std::shared_ptr<KvStoreInterface> other) = 0;
};

#endif //KEY_VALUE_STORE_KVSTOREINTERFACE_H
