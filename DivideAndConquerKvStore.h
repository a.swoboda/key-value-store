//
// Created by Andreas on 20/09/2021.
//

#ifndef KEY_VALUE_STORE_DIVIDEANDCONQUERKVSTORE_H
#define KEY_VALUE_STORE_DIVIDEANDCONQUERKVSTORE_H

#include "KvStoreInterface.h"

#include "FactoryInterface.h"

#include <map>
#include <shared_mutex>



class DivideAndConquerKvStore : public KvStoreInterface {
    std::shared_mutex mutable mutex = {};

    std::size_t const maxElementsPerChild;
    std::size_t const mergeThreshold;

    using ReadGuard = std::shared_lock<std::shared_mutex>;
    using FullGuard = std::lock_guard<std::shared_mutex>;

    using Hasher = std::hash<std::string>;
    Hasher const hasher = {};
    using KvStoreFactory = std::unique_ptr<FactoryInterface<std::unique_ptr<KvStoreExtendedInterface>, std::size_t>>;
    KvStoreFactory factory;

    // NB we might want to have one shared_mutex per child later
public:
    using Children = std::map<std::size_t, std::shared_ptr<KvStoreExtendedInterface>>;
private:
    Children children;

    using Hash = std::size_t;  // NB To be conforming, this has to be unsigned, so that overflow is well-defined
    // assume we have at least a read lock
    Children::const_iterator getChild(Hash hash) const;

    static std::size_t hashBetween(Hash lhs, Hash rhs);

    Children::const_iterator  nextOrFirstChild(Children::const_iterator child) const;

    Children::const_iterator prevOrLastChild(Children::const_iterator child) const;

    // assume we have the full lock
    Children::const_iterator getChildAndRebalanceIfNecessary(Hash hash);

public:
    DivideAndConquerKvStore(std::size_t maxPerChild, std::size_t mergeThreshold, KvStoreFactory factory);

    std::size_t size() const override;

    std::optional<std::string> get(std::string const& key) const override;

    void put(std::string const& key, std::string const& val) override;

    void erase(std::string const& key) override;
};


#endif //KEY_VALUE_STORE_DIVIDEANDCONQUERKVSTORE_H
