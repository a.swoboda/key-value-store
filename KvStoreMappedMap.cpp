//
// Created by Andreas on 20/09/2021.
//

#include "KvStoreMappedMap.h"

KvStoreMappedMap::KvStoreMappedMap(KvStoreMappedMap::MappedFile &&file) :
    mappedFile(std::move(file)),
    data(*mappedFile.find_or_construct<MapType>(OBJECT_NAME)(std::less<StringType>{}, PairAllocator(mappedFile.get_segment_manager())))
{}

std::size_t KvStoreMappedMap::size() const {
    return data.size();
}

std::optional<std::string> KvStoreMappedMap::get(const std::string &key) const {
    auto it = data.find(StringType(key.c_str(), CharAllocator{mappedFile.get_segment_manager()}));
    return it == data.end() ? std::optional<std::string>{} : it->second.c_str();
}

void KvStoreMappedMap::put(const std::string &key, const std::string &val) {
    auto insertKey = StringType(key.c_str(), CharAllocator{mappedFile.get_segment_manager()});
    auto insertVal = StringType(val.c_str(), CharAllocator{mappedFile.get_segment_manager()});
    data.insert_or_assign(std::move(insertKey), std::move(insertVal));
}

void KvStoreMappedMap::erase(const std::string &key) {
    auto eraseKey = StringType(key.c_str(), CharAllocator{mappedFile.get_segment_manager()});
    data.erase(eraseKey);
}

void KvStoreMappedMap::filter(std::function<bool(const std::string &)> predicate,
                              std::shared_ptr<KvStoreInterface> other) {
    for (auto it = begin(data); it != end(data); /**/) {
        if (predicate(it->first.c_str())) {
            other->put(it->first.c_str(), it->second.c_str());
            it = data.erase(it);
        }
        else {
            ++it;
        }
    }
}

void KvStoreMappedMap::addAllTo(std::shared_ptr<KvStoreInterface> other) {
    for (auto&& p : data) {
        other->put(p.first.c_str(), p.second.c_str());
    }
}

MappedMapFactory::MappedMapFactory(std::string name, std::size_t filesize) :
    filename(std::move(name))
{
    namespace interp = boost::interprocess;
    interp::managed_mapped_file(interp::create_only, filename.c_str(), filesize);
}

std::unique_ptr<KvStoreExtendedInterface> MappedMapFactory::construct() const {
    return std::make_unique<KvStoreMappedMap>(boost::interprocess::managed_mapped_file(boost::interprocess::open_only, filename.c_str()));
}

MappedMapFactory::~MappedMapFactory() {
    boost::interprocess::file_mapping::remove(filename.c_str());
}
