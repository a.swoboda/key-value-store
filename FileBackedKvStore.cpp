//
// Created by Andreas on 19/09/2021.
//

#include "FileBackedKvStore.h"

#include <random>

std::unique_ptr<KvStoreExtendedInterface> FileBackedKvStore::getProxy() const {
    auto result = mapFactory->construct();  // (boost::interprocess::managed_mapped_file(boost::interprocess::open_only, filename.c_str()));
    // flush all pending changes
    for (auto&& key : eraseCache) {
        result->erase(key);
    }
    eraseCache.clear();
    for (auto&& p : writeCache) {
        result->put(p.first, p.second);
    }
    writeCache.clear();
    writtenSize = result->size();
    return result;
}

void FileBackedKvStore::evictOneFromReadCache() const {
    if (readCache.empty()) { return; }
    // get a random, non-empty bucket
    auto rd = std::random_device{};
    auto const nBuckets = readCache.bucket_count();
    auto const bucketIndexDistribution = std::uniform_int_distribution<std::size_t>(0, nBuckets-1);
    auto bucketIndex = bucketIndexDistribution(rd);
    while (readCache.bucket_size(bucketIndex) == 0) {
        bucketIndex = bucketIndexDistribution(rd);
    }
    // get and erase a random element from the bucket
    auto const bucketSize = readCache.bucket_size(bucketIndex);
    auto const insideBucketDistribution = std::uniform_int_distribution<std::size_t>(0, bucketSize-1);
    readCache.erase(std::next(readCache.begin(bucketIndex), insideBucketDistribution(rd)));
}

FileBackedKvStore::FileBackedKvStore(FileBackedKvStore::ImplHandleFactory factory, std::size_t readSize,
                                     std::size_t mutateSize) :
    maxElementsRead(readSize),
    maxElementsMutate(mutateSize),
    mapFactory(std::move(factory))
{}

std::size_t FileBackedKvStore::size() const {
    auto lockGuard = ReadGuard(mutex);
    return writtenSize;
}

std::optional<std::string> FileBackedKvStore::get(const std::string &key) const {
    if (auto lockGuard = ReadGuard(mutex); eraseCache.count(key)) { return {}; }
    else if (auto const writeIt = writeCache.find(key); writeIt != end(writeCache)) {
        return writeIt->second;
    }
    else if (auto const readIt = readCache.find(key); readIt != end(readCache)) {
        return readIt->second;
    }
    auto lockGuard = FullGuard(mutex);
    if (readCache.size() >= maxElementsRead) {
        evictOneFromReadCache();
    }
    auto result = getProxy()->get(key);
    readCache.insert_or_assign(key, result);
    return result;
}

void FileBackedKvStore::put(const std::string &key, const std::string &val) {
    auto lockGuard = FullGuard(mutex);
    auto const [readIt, didInsert] = writeCache.insert_or_assign(key, val);
    if (didInsert) {
        eraseCache.erase(key);
        readCache.erase(key);
    }
    if (eraseCache.size() + writeCache.size() > maxElementsMutate) {
        getProxy();
    }
}

void FileBackedKvStore::erase(const std::string &key) {
    auto lockGuard = FullGuard(mutex);
    auto const [_, didInsert] = eraseCache.emplace(key);
    if (didInsert) {
        readCache.erase(key);
        writeCache.erase(key);
    }
    if (eraseCache.size() + writeCache.size() > maxElementsMutate) {
        getProxy();
    }
}

void
FileBackedKvStore::filter(std::function<bool(const std::string &)> predicate, std::shared_ptr<KvStoreInterface> other) {
    auto lockGuard = FullGuard(mutex);
    getProxy()->filter(predicate, other);
}

void FileBackedKvStore::addAllTo(std::shared_ptr<KvStoreInterface> other) {
    auto lockGuard = FullGuard(mutex);
    getProxy()->addAllTo(other);
}
